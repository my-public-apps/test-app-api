<?php

namespace App\Controller\API;

use App\Entity\Group;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route ("/api/group")
 */
class GroupController extends \App\Controller\ApiController
{
    const ENTITY_CLASS = Group::class;

    /**
     * @param int $id
     * @return Response
     *
     * @Route ("/get/{id}", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function getItem(int $id): Response
    {
        return new Response();
    }

    /**
     * @return Response
     *
     * @Route ("/get/list", methods={"GET"})
     */
    public function getList(): Response
    {
        return new Response();
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @Route ("/create", methods={"POST"})
     */
    public function createItem(Request $request): Response
    {
        return new Response();
    }

    /**
     * @param int $id
     * @param Request $request
     * @return Response
     *
     * @Route ("/update/{id}", methods={"PATCH"}, requirements={"id"="\d+"})
     */
    public function updateItem(int $id, Request $request): Response
    {
        return new Response();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @Route ("/delete/{id}", methods={"DELETE"}, requirements={"id"="\d+"})
     */
    public function deleteItem(int $id): Response
    {
        return new Response();
    }
}