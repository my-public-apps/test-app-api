<?php

namespace App\Controller\API;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends \App\Controller\ApiController
{

    const ENTITY_CLASS = User::class;

    /**
     * @return Response
     *
     * @Route ("/", methods={"GET"})
     */
    public function getItem(): Response
    {
        return new Response('hallow');
    }
}